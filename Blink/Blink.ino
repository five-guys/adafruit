#include <OneWire.h>
#include <DallasTemperature.h>

#include <Arduino.h>
#include <SPI.h>
#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"

#include "BluefruitConfig.h"

#define FACTORYRESET_ENABLE         1
#define MINIMUM_FIRMWARE_VERSION    "0.6.6"
#define MODE_LED_BEHAVIOUR          "MODE"

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}


#define LED_MOTOR_PIN           12
#define LED_PELTIER_PIN         11
#define LED_FREEZER_PIN         10

#define RELAY_PIN_1             6
#define RELAY_PIN_2             5

#define TEMP_PIN                A0
OneWire oneWire(TEMP_PIN);
DallasTemperature sensors(&oneWire);

DeviceAddress Thermometer;
int deviceCount = 0;
uint8_t sensor_box[8]   = { 0x28, 0x54, 0x12, 0x97, 0x32, 0x14, 0x01, 0x6C };
uint8_t sensor_water[8] = { 0x28, 0xAA, 0x02, 0x5C, 0x1D, 0x13, 0x02, 0x9B };


#define BUTTON_MOTOR_PIN        15
#define BUTTON_PELTIER_PIN      16
#define BUTTON_FREEZER_PIN      17


#define FREEZER_TEMP_VALUE_HIGH 40
#define FREEZER_TEMP_VALUE_LOW  30
#define TEMP_TIMER_RESET_TIME   30000

float temp_value;
float temperature;
long temp_timer;

int button_motor_state;
int button_peltier_state;

int motor_state;
int peltier_state;

int button_freezer_prev_val;
int freezer_state;

bool init_connect = true;


// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(LED_MOTOR_PIN, OUTPUT);
  pinMode(LED_PELTIER_PIN, OUTPUT);
  pinMode(LED_FREEZER_PIN, OUTPUT);

  pinMode(RELAY_PIN_1, OUTPUT);
  pinMode(RELAY_PIN_2, OUTPUT);
  
  pinMode(TEMP_PIN, INPUT);
  
  pinMode(BUTTON_MOTOR_PIN, INPUT_PULLUP);
  pinMode(BUTTON_PELTIER_PIN, INPUT_PULLUP);
  pinMode(BUTTON_FREEZER_PIN, INPUT_PULLUP);

  Serial.begin(115200);
  //while(! Serial);
  

  digitalWrite(LED_MOTOR_PIN, LOW);
  digitalWrite(LED_PELTIER_PIN, LOW);
  digitalWrite(LED_FREEZER_PIN, LOW);
  digitalWrite(RELAY_PIN_1, HIGH);
  digitalWrite(RELAY_PIN_2, HIGH);

  temp_value = 0;
  temperature = 0;
  temp_timer = millis() - TEMP_TIMER_RESET_TIME;
  
  button_motor_state = 0;
  button_peltier_state = 0;

  motor_state = 0;
  peltier_state = 0;

  button_freezer_prev_val = LOW;
  freezer_state = 0;

  //sensors.setResolution(12);
  sensors.begin();
  deviceCount = sensors.getDeviceCount();
  Serial.println(deviceCount);
  // find Sensor Addresses
  for(int i = 0; i< deviceCount; i++){
    sensors.getAddress(Thermometer, i);
    printAddress(Thermometer);  
  }
  /*
  sensors.requestTemperatures();
  temp_value = sensors.getTempC(sensor_water);
  temperature = sensors.getTempC(sensor_box);
  printTemperature();
  */
  if (!ble.begin(VERBOSE_MODE) ){
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  if (FACTORYRESET_ENABLE){
    Serial.println(F("Performing a factory reset: "));
    if (!ble.factoryReset()){
      error(F("Couldn't factory reset"));
    }
  }
  ble.echo(false);
  ble.info();
  ble.verbose(false);
  /*
  while (!ble.isConnected()) {
      delay(500);
  }
  */
  if (ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION)) {
    ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);
  }
  ble.setMode(BLUEFRUIT_MODE_DATA);
  
  
  Serial.println("Setup finish");
}

// the loop function runs over and over again forever
void loop() {

  button_motor_state = digitalRead(BUTTON_MOTOR_PIN);
  button_peltier_state = digitalRead(BUTTON_PELTIER_PIN);
  //temp_value = analogRead(TEMP_PIN);

  long delta_timer = millis()- temp_timer - TEMP_TIMER_RESET_TIME;
  /*
  Serial.print("Temperatur set countdown: ");
  Serial.print(-delta_timer);
  Serial.print("          Last Temperatur: ");
  Serial.println(temp_value);
  */
  if(delta_timer >= 0){
    sensors.requestTemperatures();
    temp_value = sensors.getTempC(sensor_water);
    temperature = sensors.getTempC(sensor_box);
    if(ble.isConnected()){
      String data = "t " + String(temperature);
      ble.print(data);
    }
    printTemperature();
    temp_timer = millis() - delta_timer;  
  }
  
  

  int button_feezer_val = digitalRead(BUTTON_FREEZER_PIN);

  //printStates(button_feezer_val);

  if(button_feezer_val == HIGH){
    if(button_freezer_prev_val == LOW){
      freezer_state = (freezer_state+1) % 2;
      digitalWrite(LED_FREEZER_PIN, freezer_state == 1 ? HIGH:LOW);
      if(ble.isConnected()){
        String data = "f " + String(freezer_state);
        ble.print(data);
      }
      button_freezer_prev_val = HIGH;
    }
  }
  else {
     button_freezer_prev_val = LOW;
  }


  if(freezer_state == 1){
    setPeltier(HIGH);

    if(temp_value > FREEZER_TEMP_VALUE_HIGH){
      setMotor(HIGH);
    }
    else if(temp_value < FREEZER_TEMP_VALUE_LOW){
      setMotor(LOW);
    }
  }
  else {
    if(button_peltier_state == HIGH || peltier_state == 1) {
      setPeltier(HIGH);
    }
    else{
      setPeltier(LOW);
    }

    if(button_motor_state == HIGH ||motor_state == 1) {
      setMotor(HIGH);
    }
    else{
      setMotor(LOW);
    }
  }  

  if(ble.isConnected()){
    if(init_connect){
      delay(1000);
      String data = "t " + String(temperature);
      ble.print(data);
      init_connect = false;
    }
    
    int command = 0;
    while (ble.available())
    {
      char c = ble.read();
      if(c == 'R')
        command = -command;
      else if(c != 10)
        command = (command * 10) + c - '0';
    }
    if(command != 0)
      Serial.println(command);
      
    if(command == BUTTON_FREEZER_PIN){ // 17
      freezer_state = (freezer_state+1) % 2;
      digitalWrite(LED_FREEZER_PIN, freezer_state == 1 ? HIGH:LOW);
      String data = "f " + String(freezer_state);
      ble.print(data);
    }
    else if(command == BUTTON_MOTOR_PIN) // 15
      motor_state = (motor_state+1) % 2;
    else if(command == BUTTON_PELTIER_PIN) // 16
      peltier_state = (peltier_state+1) % 2;
  }
  else {
    init_connect = true;
  } 
}

void setMotor(int val){
  digitalWrite(LED_MOTOR_PIN, val);
  val = val == 0 ? HIGH : LOW; 
  digitalWrite(RELAY_PIN_1, val);
}

void setPeltier(int val){
  digitalWrite(LED_PELTIER_PIN, val);
  val = val == 0 ? HIGH : LOW;
  digitalWrite(RELAY_PIN_2, val);
}

void printAddress(DeviceAddress deviceAddress){
  for(uint8_t i = 0; i < 8; i++){
    Serial.print("0x");
    if(deviceAddress[i] < 0x10) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
    if(i < 7) Serial.print(", ");
  }  
  Serial.println("");
}

void printTemperature(){
    Serial.print("Temperature in Water is: "); 
    Serial.print(temp_value);
    Serial.print("     Temperature in Box is: "); 
    Serial.println(temperature);
}

void printStates(int button_feezer_val){
  Serial.print("button_motor_state: ");
  Serial.print(button_motor_state);
  Serial.print("     button_peltier_state: ");
  Serial.print(button_peltier_state);
  Serial.print("     button_freezer_state: ");
  Serial.print(button_feezer_val);
  Serial.print("     freezer_state: ");
  Serial.println(freezer_state);
}
