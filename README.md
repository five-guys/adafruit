# Adafruit

Implementation for *Adafruit Feather M0 Bluefruit LE*, that was integrated in the In-&-Out-Box for peltier & fan control, as well as communication with the smartphone app.